package com.rbs.circulation;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OverwatchController {

	@RequestMapping("/stats")
    public String stats() {
        return "Greetings from Spring Boot Stats!";
	}
}
